# Reescribe el programa del salario usando try y except, de modo que el programa sea capaz de gestionar
#  entradas no numéricas con elegancia, mostrando un mensaje y saliendo del programa. A continuación se
#  muestran dos ejecuciones del programa:

#  autora  "Karen Paladines"
#  email   "karen.paladines@unl.edu.ec"

try:
    horas = int(input("Ingrese las horas:"))
    tarifa = float(input("Ingrese la tarifa por hora:"))

    if horas > 40:
        resto = horas - 40
        extra = (resto * 1.5) * tarifa
        salario = (40 * tarifa) * extra
        print("El salario es:", salario)

    else:
        salario = horas * tarifa
        print("El salario es:",salario)

except:
    print("Error, por favor ingrese un número")
