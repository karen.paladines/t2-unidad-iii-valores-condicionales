#  Reescribe el programa del cálculo del salario para darle al empleado 1.5 veces la tarifa
#  horaria para todas las horas trabajadas que excedan de 40.
#  autora  "Karen Paladines"
#  email   "karen.paladines@unl.edu.ec"

horas = int(input("Ingrese las horas:"))
tarifa = float(input("Ingrese la tarifa:"))

if horas > 40:
   horas_xt=horas-40
   tarifa_xt=(1.5*horas_xt)*tarifa
   salario = (40*tarifa_xt)+ tarifa_xt
   print("El salario es:", salario)

else:
   salario = horas * tarifa
   print("El salario es:", salario)